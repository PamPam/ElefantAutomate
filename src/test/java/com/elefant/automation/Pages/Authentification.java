package com.elefant.automation.Pages;

import com.elefant.automation.Utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SourceType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import static com.elefant.automation.Utils.BaseTest.driver;

public class Authentification {

    @FindBy(how = How.XPATH, using = ".//*[@id='login_username']")
    private WebElement UserName;
    @FindBy(how = How.XPATH, using = ".//*[@id='login_password']")
    private WebElement Password;
    @FindBy(how = How.XPATH, using = "//*[@id=\"login_classic\"]")
    private WebElement LoginButton;
    @FindBy(how = How.XPATH, using = "/html/body/div[4]/header/div[1]/div[3]/a/img[1]")
    private WebElement MainLogo;
    @FindBy(how = How.XPATH, using = "/html/body/div[4]/header/div[1]/div[3]/a/img[1]")
    private WebElement GoogleLogin;
    @FindBy(how = How.ID, using = "identifierId")
    private WebElement Email;
    @FindBy(how = How.CLASS_NAME, using = "CwaK9")
    private WebElement Next;
    @FindBy(how = How.NAME, using = "password")
    private WebElement Pass;
    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[3]/div[1]/div[2]/div[2]/a")
    private WebElement Adress;
    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[3]/div[3]/div/div[1]/div[2]/div/a")
    private WebElement AddAdress;
    @FindBy(how = How.CSS, using = ".form-control.new_user_address_input")
    private WebElement TextField;
    @FindBy(how = How.CSS, using = "#firstname")
    private WebElement FirstName;
    @FindBy(how = How.CSS, using = "#lastname")
    private WebElement LastNamex;
    @FindBy(how = How.CSS, using = "#email")
    private WebElement Email2;
    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[3]/div[3]/div/div[5]/form/input")
    private WebElement Save;
    @FindBy(how = How.XPATH, using = "(//SELECT[@class='form-control'])[1]")
    public static WebElement BirthDay;
    @FindBy(how = How.XPATH, using = "(//SELECT[@class='form-control'])[2]")
    private WebElement BirthMonth;
    @FindBy(how = How.XPATH, using = "(//SELECT[@class='form-control'])[3]")
    private WebElement Birthyear;


    /**
     * Method for generating a random birth date
     */
    public void generateBirthDate(){
        Select MyBirthDay = new Select(BirthDay);
        MyBirthDay.selectByIndex( Utils.random(1, 30));
        Utils.delay(1000);
        Select MyBirthMonth = new Select(BirthMonth);
        MyBirthMonth.selectByIndex( Utils.random(1, 12));
        Select MyBirthYear= new Select(Birthyear);
        MyBirthYear.selectByValue(Integer.toString(Utils.random(1930, 2000)));
    }
    /**
     * Simple method to verify the existence of the button in page and pressing it.
     */
    public void savingData(){
        Assert.assertTrue(Save.isDisplayed());
        Save.click();
    }
    /**
     * Method for introducing first name, last name, email and birth date.
     * @param: dragos, avadanei, pampam0237@gmail.com, 28.01.1990
     * after introducing data we are using assert to verify every param
     */
    public void modifyData(){
        FirstName.click();
        FirstName.clear();
        FirstName.sendKeys("dragos");
        LastNamex.click();
        LastNamex.clear();
        LastNamex.sendKeys("avadanei");
        Email2.click();
        Email2.clear();
        Email2.sendKeys("pampam0237@gmail.com");
    }
    /**
     * Method for clicking AddAdress button
     * Wait for 1 sec, perform an assert for the existence of the element, click the button
     */
    public void clickOnAddAdress(){
        Utils.delay();
        Assert.assertTrue(AddAdress.isDisplayed());
        AddAdress.click();
    }
    /**
     * Method for clicking Adress button from left vertical menu
     * Wait for 1 sec, perform an assert for the existence of the element, click the button
     */
    public void clickOnAdress(){
        Utils.delay();
        Assert.assertTrue(Adress.isDisplayed());
        Adress.click();
    }
    /**
     * Method for verifying the page adress
     */
    public void verifyPageAdress(){
        Utils.delay();
        Assert.assertEquals(driver.getCurrentUrl(), "http://www.elefant.ro/contul-meu/setari-cont");
    }
    /**
     * Method for inserting the gmail adress
     */
    public void insertGmailData(){
    Utils.delay(2000);
    Email.click();
    Email.clear();
    Email.sendKeys("automation.test0123@gmail.com");
    Next.click();
    Assert.assertTrue(Next.isDisplayed());
}
    /**
     * Method for inserting the gmail password
     */
    public void insertGmailPass(){
        Utils.delay(2000);
        Assert.assertTrue(Pass.isDisplayed());
        Pass.click();
        Pass.clear();
        Pass.sendKeys("QWErty123");
        Next.click();
    }
    /**
     * Method for connectiong to google account
     */
    public void clickOnGoogle() {
        GoogleLogin.click();
        Utils.delay(3000);
        Assert.assertTrue(Email.isDisplayed());
    }
    /**
     * Method for clasical connect to user account
     * @paramUser: pampam0237@gmail.com
     * @pass: 123456
     *
     */
    public void insertElefantData(){
        Assert.assertEquals(driver.getCurrentUrl(),"http://www.elefant.ro/autentificare");
        Utils.delay();
        UserName.click();
        UserName.sendKeys("pampam0237@gmail.com");
        Password.click();
        Password.sendKeys("123456");
    }
    /**
     * Method for clicking login button
     */
    public void clickOnLogin() {
        Assert.assertTrue(LoginButton.isDisplayed());
        LoginButton.submit();
    }
    /**
     * Method for clicking MainLogo that has 2 version depending in what page is the user
     */
    public void clickOnMainLogo(){
        Utils.delay();
        try{MainLogo.click();}
        catch (Exception e){System.out.println("first version main logo failed ! ");}
    }

}
