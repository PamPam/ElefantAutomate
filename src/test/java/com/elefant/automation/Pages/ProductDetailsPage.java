package com.elefant.automation.Pages;

import com.elefant.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class ProductDetailsPage {
    @FindBy(how= How.CSS, using = ".product-adauga-in-cos-text")
    private WebElement AddToBasket;
    @FindBy(how= How.XPATH, using = ".//*[@id='add-to-wishlist']")
    private WebElement AddToWishlist;
    @FindBy(how = How.XPATH, using = ".//*[@class='wishlist-add']")
    private WebElement CreateWH;
    @FindBy(how= How.XPATH, using = ".//*[@id='cart_count']")
    private WebElement CartButton;
    @FindBy(how= How.XPATH, using = ".//*[@id='cart_count']")
    private WebElement Wishlist;

    /**
     * This method will create a wishlist
     */
    public void clickCreateWH(){
        Utils.delay();
        Assert.assertTrue(CreateWH.isDisplayed());
        CreateWH.click();
        Utils.delay();
    }
    /**
     * This method clicks on "adauga in cos"
     */
    public void clickAddToBasket(){
        Utils.delay();
        Assert.assertTrue(AddToBasket.isDisplayed());
        AddToBasket.click();
    }
    /**
     * This method clicks on "wishlist"
     */
    public void clickWishlist(){
        Utils.delay();
        Assert.assertTrue(AddToWishlist.isDisplayed());
        AddToWishlist.click();
        Utils.delay(2000);
    }
    /**
     * This method clicks on "basket"
     */
    public void clickOnCartButton(){
        Assert.assertTrue(CartButton.isDisplayed());
        Utils.delay();
        CartButton.click();
    }


}
