package com.elefant.automation.Pages;

import com.elefant.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class BasketPage {

    @FindBy(how = How.LINK_TEXT, using = "Sterge")
    private WebElement StergeDinCos;

    /**
     * This method clicks on "sterge"
     */
    public void deleteFromBasket(){
        Utils.delay();
        Assert.assertTrue(StergeDinCos.isDisplayed());
        StergeDinCos.click();
    }


}
