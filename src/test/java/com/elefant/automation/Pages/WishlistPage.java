package com.elefant.automation.Pages;

import com.elefant.automation.Utils.Utils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class WishlistPage {

    @FindBy(how = How.XPATH, using = "//*[@class='wishlistRemoveItem']")
    private WebElement DeleteFromWishlistButton;

    public void clearWH() {
        Assert.assertTrue(DeleteFromWishlistButton.isDisplayed());
        try {
            do {
                Utils.delay(2000);
                DeleteFromWishlistButton.click();
                Utils.delay(2000);
            }
            while (DeleteFromWishlistButton.isDisplayed());
        } catch (NoSuchElementException InexistentElement) {
            System.out.println("No such element !");
        }
    }
}
