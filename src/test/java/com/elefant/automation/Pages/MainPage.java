package com.elefant.automation.Pages;

import com.elefant.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

import static com.elefant.automation.Utils.BaseTest.driver;

public class MainPage {

    @FindBy(how = How.CSS, using = ".hidden-xs.header-account-display")
    private WebElement ContulMeuButton;
    @FindBy(how = How.CSS, using = ".not_logged_in")
    private WebElement IntraInContButton;
    @FindBy(how = How.XPATH, using = "html/body/div[2]/div/div[1]/a")
    private WebElement BackToPage;
    @FindBy(how = How.XPATH, using = ".//*[@id='query']")

    private WebElement SearchBox;
    @FindBy(how = How.XPATH, using = "//*[@id=\"query\"]")

//    private WebElement SearchBox;
//    @FindBy(how = How.XPATH, using = ".//*[@id='user_name']")

    private WebElement UserName;
    @FindBy(how = How.ID, using = "searchsubmit")
    private WebElement SearchButton;
    @FindBy (how=How.XPATH, using = "html/body/div[4]/header/div[2]/div[6]/div/div/ul/li")
    public List<WebElement> OrizontalMenu;
    @FindBy (how=How.XPATH, using = "html/body/div[4]/header/div[2]/div[6]/div/div/ul/li[1]/div/div/div[1]/ul/li")
    public List<WebElement> ParfumuriCosmeticeSubmenu;
    @FindBy (how=How.XPATH, using = "html/body/div[4]/div[1]/div[1]/div[2]/div[4]/div")
    public List<WebElement> ParfumuriFromSearch;
    @FindBy (how=How.LINK_TEXT, using = "Apa de parfum Wish, 75 ml, Pentru Femei")
    public WebElement ParfumuriFromSearch2;
    @FindBy(how = How.ID, using = "logout-button")
    private WebElement LogOutButton;
    @FindBy(how = How.CSS, using = ".cont_detaliu.not_logged_in")
    private WebElement Wishlist;
    @FindBy(how= How.ID, using = "add-to-cart")
    private WebElement AddToBasket;
    @FindBy(how = How.XPATH, using = "html/body/div[4]/header/div[1]/div[4]/div[4]/ul/li[6]/a")
    private WebElement AccountSetUp;



    /**
     * This method clicks on Account SetUp button then
     * Verifies if a sub menu button exists and
     * If it is present will click on it and print a specific string in console
     */
    public void clickOnAccountSetUp(){
        Utils.delay();
        Assert.assertTrue(AccountSetUp.isDisplayed());
        AccountSetUp.click();
        System.out.println("Open Account SetUp !");
    }
    /**
     * This method clicks on wishlist button then
     * Verifies if a sub menu button exists and
     * If it is present will click on it and print a specific string in console
     */
    public void clickOnWishlist(){
        Assert.assertTrue(Wishlist.isDisplayed());
        Wishlist.click();
        System.out.println("Open Wishlist !");
    }
    /**
     * This method clicks on a button then
     * Verifies if a sub menu button exists and
     * If it is present will click on it
     */
    public void logout(){
        Utils.delay();
        try {ContulMeuButton.click();
            Assert.assertTrue(LogOutButton.isDisplayed());
            LogOutButton.click();
            System.out.println("Log out successfull !");
        }
        catch (AssertionError e){
            System.out.println("Log out procedure failed !");
        }
    }
    /**
     * This method clicks on first search result from page
     */
    public void verifyPageTitle(){
        Assert.assertEquals(driver.getCurrentUrl(), "http://www.elefant.ro/list/cosmetice-si-parfumuri/parfumuri/apa-de-parfum?sortsales=desc&filtersex=Femei~~~Unisex#__scGFtcGFtMDIzN0BnbWFpbC5jb20");
   }
    /**
     * Simple method that clicks on first search result from list
     */
    public void clickOnFirstSearchResult(){
        Assert.assertTrue(ParfumuriFromSearch2.isDisplayed());
        ParfumuriFromSearch2.click();
    }
    /**
     * This method verifies if the add to basket button is displayed in page
     */
    public void detailsPageVerification(){
        Utils.delay(3000);
        Assert.assertTrue(AddToBasket.isDisplayed());
    }
    /**
     * This method verifies if the elements from list contains a specific text in their name
     * if yes will return the position from list
     * if no it will return -1
     */
    public int menuValidation(String x){
        for(int i=0; i<OrizontalMenu.size(); i++){
            if(OrizontalMenu.get(i).getText().contains(x)){
            return i;
            }
        }
        return -1;
    }
    /**
     * This method tries to click on the "Category"  button
     * If a specific text is displayed
     * The verification is made in a previous method "menuValidation"
     */
    public void clickOnCategory(String z){
        int pos = menuValidation(z);
        if (pos!=-1){
            OrizontalMenu.get(pos).click();
        }else {
            System.out.println("Can't press on Orizontal menu button !");
        }
        Assert.assertTrue(OrizontalMenu.indexOf(pos)==-1);

    }
    /**
     * This method verifies if the elements from list contains a specific text in their name
     * if yes will return the position from list
     * if no it will return -1
     */
    public int subMenuValidation(String x){
           for(int i=0; i<ParfumuriCosmeticeSubmenu.size(); i++){
               if(ParfumuriCosmeticeSubmenu.get(i).getText().equals(x)){
                   return i;
            }
        }
        return -1;
    }
    /**
     * This method tries to click on the "SubMenu"  button
     * If a specific text is displayed
     * The verification is made in a previous method "subMenuValidation"
     */
    public void clickOnSubMenu(String z){
        int pos = subMenuValidation(z);
        if (pos!=-1){
            ParfumuriCosmeticeSubmenu.get(pos).click();
        }else {
            System.out.println("Can't press on SubOrizontal menu button !");
        }
        Assert.assertTrue(OrizontalMenu.indexOf(pos)==-1);
    }
    /**
     * This method tries to click on the "ContulMeu"  button
     * If button is not present a message will be displayed
     */
    public void clickOnContulMeu(){
        Utils.delay(2000);
        Assert.assertTrue(ContulMeuButton.isDisplayed());
        try {
        ContulMeuButton.click();
    }
        catch (AssertionError e){
        System.out.println("ContulMeu button is not available !");
    }
    }
    /**
     * This method tries to click on the "IntraInCont"  button after 1 sec wait
     * If button is not present a message will be displayed
     */
    public void clickOnIntraInCont(){
        Utils.delay();
        Assert.assertTrue(IntraInContButton.isDisplayed());
        try {
            IntraInContButton.click();
        }
        catch (AssertionError e){
            System.out.println("IntraInCont button is not available !");
        }
    }
    /**
     * This method waits for 1 sec then tries to click on the back to page button
     * If button is not present a message will be displayed
     */
    public void backToPageButton(){
        Utils.delay();
        try {
            BackToPage.click();
        }
        catch (Exception e){
            System.out.println("BackToPageButton is not available !");
        }
    }
    /**
     * This method verifies if the loged user is the correct user
     */
    public void verifyLogedUser(){
        Assert.assertEquals(UserName.getText(), "dragos");
        if (UserName.getText().equals("dragos")){
            System.out.println("Username correct !");
        }
            else{
        System.out.println("Wrong user name !");}
    }
    /**
     * This method verifies if the loged user is the correct user
     */
    public void verifyLogedUser2(){
        Assert.assertEquals(UserName.getText(), "gfdgfsgdsfg");
        if (UserName.getText().equals("gfdgfsgdsfg")){
            System.out.println("Username correct !");
        }
        else{
            System.out.println("Wrong user name !");}
    }
    /**
     * This method will click on search box field box and insert a specific string
     * @param: hugo boss
     */
    public String clickOnSearchBox(String x) {
        Utils.delay();
        Assert.assertTrue(SearchBox.isDisplayed());
        try {
            SearchBox.click();
        } catch (AssertionError e) {
            System.out.println("Search box is not available !");
        }
        SearchBox.sendKeys(x);
        return x;
    }
    /**
     * This method tries to click on the "searching"  button
     * If button is not present a message will be displayed
     */
    public void clickOnSearchButton(){
        Utils.delay();
        Assert.assertTrue(SearchButton.isDisplayed());
        try {
            SearchButton.click();
        }
        catch (AssertionError e){
            System.out.println("Searching button is not available !");
        }
    }

}
