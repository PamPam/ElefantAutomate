package com.elefant.automation.Pages;

import com.elefant.automation.Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class SearchResultPage {
    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[1]/div[1]/div[2]/div[1]/div/h1/span[1]")
    private WebElement SearchResultText;
    @FindBy(how = How.XPATH, using = ".//*[@id='101691018']/div/div[2]/div/div[2]/a")
    private WebElement FirstElement;
    @FindBy(how = How.LINK_TEXT, using = "Ceas Fossil Machine FS4487")
    private WebElement LinkElementFossil;
    @FindBy(how = How.LINK_TEXT, using = "Ceas Hugo Boss Supernova 1513361")
    private WebElement LinkElementHugo;
    @FindBy(how = How.CSS, using = ".hidden-xs.show-selected-filter")
    private WebElement OrderByButton;
    @FindBy(how = How.XPATH, using = ".//*[@id='elf-sort-by-list']/li[3]/a")
    private WebElement Descrescator;
//    #elf-sort-by-list>li>a


    /**
     * This method verifies if the searched text is displayed in page
     * If the verification fails a specific message will be displayed
     */
    public void verifySearchResults(String x){
        try {
            Assert.assertEquals(SearchResultText.getText(), x);
            System.out.println("Search succesfull !");
        }
        catch (Exception e){
            System.out.println("Result verification failed !");
        }
    }
    /**
     * This method clicks on first entry from search result
     */
    public void clickOnFirstResultFosil(){
        Utils.delay();
        Assert.assertTrue(LinkElementFossil.isDisplayed());
        LinkElementFossil.click();
    }
    /**
     * This method clicks on first entry from search result
     */
    public void clickOnFirstResultHugo(){
        Utils.delay();
        LinkElementHugo.click();
    }
    /**
     * This method clicks on order by drop box selector
     */
    public void clickOnOrderByButton(){
        Utils.delay();
        Assert.assertTrue(OrderByButton.isDisplayed());
        OrderByButton.click();
    }
    /**
     * This method select descending order
     */
    public void clickOnDescrescator(){
        Utils.delay();
        Descrescator.click();
    }


}
