package com.elefant.automation.Tests;

import com.elefant.automation.Pages.*;
import com.elefant.automation.Utils.BaseTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class AddToBasketTest extends BaseTest {

    @Test
    public void AddToBasket() {

        logger = extent.startTest("AddToBasket");

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        Authentification authentific = PageFactory.initElements(driver, Authentification.class);
        SearchResultPage search = PageFactory.initElements(driver, SearchResultPage.class);
        ProductDetailsPage prod = PageFactory.initElements(driver, ProductDetailsPage.class);
        BasketPage basket = PageFactory.initElements(driver, BasketPage.class);

        PrtScreen();
        mainPage.backToPageButton();
        mainPage.clickOnContulMeu();
            logger.log(LogStatus.PASS, "Button \"Contul Meu\" is displayed !");
        PrtScreen();
        mainPage.clickOnIntraInCont();
            logger.log(LogStatus.PASS, "Authentification page is loaded !");
        PrtScreen();
        authentific.insertElefantData();
            logger.log(LogStatus.PASS, "Authentification data is inserted succesfully !");
        PrtScreen();
        authentific.clickOnLogin();
            logger.log(LogStatus.PASS, "Log in button is displayed !");
        PrtScreen();
        authentific.clickOnMainLogo();
        mainPage.backToPageButton();
            logger.log(LogStatus.PASS, "Title verified !");
        PrtScreen();
        mainPage.clickOnSearchBox("Ceas Fossil Machine FS4487");
            logger.log(LogStatus.PASS, "Insert search string verification !");
        PrtScreen();
        mainPage.clickOnSearchButton();
            logger.log(LogStatus.PASS, "Search button is displayed and functional !");
        PrtScreen();
        search.clickOnFirstResultFosil();
            logger.log(LogStatus.PASS, "Click on first search result !");
        PrtScreen();
        prod.clickAddToBasket();
            logger.log(LogStatus.PASS, "Details page loaded successfully !");
        PrtScreen();
        prod.clickOnCartButton();
            logger.log(LogStatus.PASS, "Basket button exist in main page !");
        PrtScreen();
        basket.deleteFromBasket();
            logger.log(LogStatus.PASS, "Delete button exist in basket page !");
        PrtScreen();
        authentific.clickOnMainLogo();
        mainPage.backToPageButton();
        PrtScreen();
        mainPage.logout();

    }
}
