package com.elefant.automation.Tests;

import com.elefant.automation.Pages.Authentification;
import com.elefant.automation.Pages.MainPage;
import com.elefant.automation.Pages.SearchResultPage;
import com.elefant.automation.Utils.BaseTest;
import com.elefant.automation.Utils.Utils;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

    public class SearchingTest extends BaseTest {

        @Test
        public void SearchTest() {
            logger = extent.startTest("SearchTest");

            Utils.createFolder("PictureReports");

            MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
            Authentification authentific = PageFactory.initElements(driver, Authentification.class);
            SearchResultPage search = PageFactory.initElements(driver, SearchResultPage.class);


            PrtScreen();
            System.out.println("Current working directory : " + Utils.CurrentWorkingDirectory());
            mainPage.backToPageButton();
            mainPage.clickOnContulMeu();
                logger.log(LogStatus.PASS, "Button \"Contul Meu\" is displayed !");
            PrtScreen();
            mainPage.clickOnIntraInCont();
                logger.log(LogStatus.PASS, "Authentification page is loaded !");
            PrtScreen();
            authentific.insertElefantData();
                logger.log(LogStatus.PASS, "Authentification data is inserted succesfully !");
            PrtScreen();
            authentific.clickOnLogin();
                logger.log(LogStatus.PASS, "Log in button is displayed !");
            PrtScreen();
            authentific.clickOnMainLogo();
            mainPage.backToPageButton();
                logger.log(LogStatus.PASS, "Title verified !");
            PrtScreen();
            mainPage.clickOnSearchBox("hugo boss");
                logger.log(LogStatus.PASS, "Insert search string in search box !");
            PrtScreen();
            mainPage.clickOnSearchButton();
                logger.log(LogStatus.PASS, "Search button is available !");
            PrtScreen();
            search.verifySearchResults("\"hugo boss\"");
                logger.log(LogStatus.PASS, "Verifying search results !");
            PrtScreen();
            mainPage.logout();



        }
    }
