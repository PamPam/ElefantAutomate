package com.elefant.automation.Tests;

import com.elefant.automation.Pages.*;
import com.elefant.automation.Utils.BaseTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class AddToWishlistTest extends BaseTest {

    @Test
    public void AddToWishlist() {

        logger = extent.startTest("AddToWishlist");


        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        Authentification authentific = PageFactory.initElements(driver, Authentification.class);
        SearchResultPage search = PageFactory.initElements(driver, SearchResultPage.class);
        ProductDetailsPage prod = PageFactory.initElements(driver, ProductDetailsPage.class);
        WishlistPage wh = PageFactory.initElements(driver, WishlistPage.class);

        PrtScreen();
        mainPage.backToPageButton();
        mainPage.clickOnContulMeu();
            logger.log(LogStatus.PASS, "Button \"Contul Meu\" is displayed !");
        PrtScreen();
        mainPage.clickOnIntraInCont();
            logger.log(LogStatus.PASS, "Authentification page is loaded !");
        PrtScreen();
        authentific.insertElefantData();
            logger.log(LogStatus.PASS, "Authentification data is inserted succesfully !");
        PrtScreen();
        authentific.clickOnLogin();
            logger.log(LogStatus.PASS, "Log in button is displayed !");
        PrtScreen();
        authentific.clickOnMainLogo();
        mainPage.backToPageButton();
            logger.log(LogStatus.PASS, "Title verified !");
        PrtScreen();
        mainPage.clickOnSearchBox("hugo boss");
            logger.log(LogStatus.PASS, "Search text field is displayed !");
        PrtScreen();
        mainPage.clickOnSearchButton();
            logger.log(LogStatus.PASS, "Search button is displayed !");
        PrtScreen();
        search.clickOnOrderByButton();
            logger.log(LogStatus.PASS, "OrderBy button is displayed !");
        PrtScreen();
        search.clickOnDescrescator();
        PrtScreen();
        search.clickOnFirstResultHugo();
        prod.clickWishlist();
            logger.log(LogStatus.PASS, "Add t wishlist button is displayed and available!");
        PrtScreen();
        prod.clickCreateWH();
            logger.log(LogStatus.PASS, "Create wishlist button is displayed and available !");
        PrtScreen();
        mainPage.clickOnContulMeu();
            logger.log(LogStatus.PASS, "Button \"Contul Meu\" is displayed !");
        PrtScreen();
        mainPage.clickOnWishlist();
            logger.log(LogStatus.PASS, "Wishlist button from main window is displayed and available !");
        PrtScreen();
        wh.clearWH();
            logger.log(LogStatus.PASS, "Clear all wishlist button is displayed and available !");
        PrtScreen();
        authentific.clickOnMainLogo();
        PrtScreen();
        mainPage.backToPageButton();
        PrtScreen();
        mainPage.logout();
    }
}
