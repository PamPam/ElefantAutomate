package com.elefant.automation.Tests;

import com.elefant.automation.Pages.*;
import com.elefant.automation.Utils.BaseTest;
import com.elefant.automation.Utils.Utils;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class AccountSetUpTest extends BaseTest {

    @Test
    public void AccountSetUp() {

        logger = extent.startTest("AccountSetUp");

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        Authentification authentific = PageFactory.initElements(driver, Authentification.class);
        SearchResultPage search = PageFactory.initElements(driver, SearchResultPage.class);
        ProductDetailsPage prod = PageFactory.initElements(driver, ProductDetailsPage.class);
        BasketPage basket = PageFactory.initElements(driver, BasketPage.class);

        PrtScreen();
        mainPage.backToPageButton();
        PrtScreen();
        mainPage.clickOnContulMeu();
            logger.log(LogStatus.PASS, "Button \"Contul Meu\" is displayed !");
        PrtScreen();
        mainPage.clickOnIntraInCont();
            logger.log(LogStatus.PASS, "Authentification page is loaded !");
        PrtScreen();
        authentific.insertElefantData();
            logger.log(LogStatus.PASS, "Authentification data is inserted succesfully !");
        PrtScreen();
        authentific.clickOnLogin();
            logger.log(LogStatus.PASS, "Log in button is displayed !");
        PrtScreen();
        authentific.clickOnMainLogo();
        PrtScreen();
        mainPage.backToPageButton();
            logger.log(LogStatus.PASS, "Title verified !");
        PrtScreen();
        mainPage.verifyLogedUser();
            logger.log(LogStatus.PASS, "Logged user verification !");
        PrtScreen();
        mainPage.clickOnContulMeu();
            logger.log(LogStatus.PASS, "Button \"Contul Meu\" is displayed !");
        PrtScreen();
        mainPage.clickOnAccountSetUp();
            logger.log(LogStatus.PASS, "Button \"Account SetUp\" is displayed !");
        PrtScreen();
        authentific.verifyPageAdress();
            logger.log(LogStatus.PASS, "Title verification for Account Setup  !");
        PrtScreen();
        authentific.modifyData();
            logger.log(LogStatus.PASS, "Data input and verification !");
        PrtScreen();
        authentific.generateBirthDate();
        authentific.savingData();
            logger.log(LogStatus.PASS, "Save button is displayed and functional !");
        PrtScreen();

        mainPage.logout();
        PrtScreen();

    }
}
