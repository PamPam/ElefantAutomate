package com.elefant.automation.Tests;

import com.elefant.automation.Pages.Authentification;
import com.elefant.automation.Pages.MainPage;
import com.elefant.automation.Utils.BaseTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class Authentification3PartyTest extends BaseTest {


    @Test
    public void Authentification3PartyTest() {

        logger = extent.startTest("Authentification3PartyTest");

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        Authentification authentific = PageFactory.initElements(driver, Authentification.class);

        mainPage.backToPageButton();
        PrtScreen();
        mainPage.clickOnContulMeu();
            logger.log(LogStatus.PASS, "Button \"Contul Meu\" is displayed !");
        PrtScreen();
        mainPage.clickOnIntraInCont();
            logger.log(LogStatus.PASS, "Authentification page is loaded !");
        PrtScreen();
        authentific.clickOnGoogle();
            logger.log(LogStatus.PASS, "Gmail login page is loading !");
        PrtScreen();
        authentific.insertGmailData();
            logger.log(LogStatus.PASS, "Email page is displayed !");
        PrtScreen();
        authentific.insertGmailPass();
            logger.log(LogStatus.PASS, "Email password page is displayed !");
        PrtScreen();
        mainPage.backToPageButton();
        PrtScreen();
        mainPage.verifyLogedUser2();
            logger.log(LogStatus.PASS, "Logged user id verification !");
        PrtScreen();
        mainPage.logout();
        PrtScreen();
    }
}
