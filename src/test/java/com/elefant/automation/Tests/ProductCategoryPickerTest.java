package com.elefant.automation.Tests;

import com.elefant.automation.Pages.Authentification;
import com.elefant.automation.Pages.MainPage;
import com.elefant.automation.Utils.BaseTest;
import com.elefant.automation.Utils.Utils;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class ProductCategoryPickerTest extends BaseTest {

    @Test
    public void ProductCategoryPickerTest() {

        logger = extent.startTest("ProductCategoryPickerTest");

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        Authentification authentific = PageFactory.initElements(driver, Authentification.class);

        PrtScreen();
        mainPage.backToPageButton();
        PrtScreen();
        mainPage.clickOnContulMeu();
            logger.log(LogStatus.PASS, "Button \"Contul Meu\" is displayed !");
        PrtScreen();
        mainPage.clickOnIntraInCont();
            logger.log(LogStatus.PASS, "Authentification page is loaded !");
        PrtScreen();
        authentific.insertElefantData();
            logger.log(LogStatus.PASS, "Authentification data is inserted succesfully !");
        PrtScreen();
        authentific.clickOnLogin();
            logger.log(LogStatus.PASS, "Log in button is displayed !");
        PrtScreen();
        authentific.clickOnMainLogo();
        mainPage.backToPageButton();
            logger.log(LogStatus.PASS, "Title verified !");
        PrtScreen();
        mainPage.verifyLogedUser();
            logger.log(LogStatus.PASS, "Logged user verification !");
        PrtScreen();
        mainPage.menuValidation("Parfumuri&cosmetice");
        Utils.delay();
        mainPage.clickOnCategory("Parfumuri&cosmetice");
            logger.log(LogStatus.PASS, "Parfumuri&cosmetice submenu exists !");
        Utils.delay();
        mainPage.subMenuValidation("Apa de parfum");
        Utils.delay();
        PrtScreen();
        mainPage.clickOnSubMenu("Apa de parfum");
            logger.log(LogStatus.PASS, "Apa de parfum submenu exists !");
        Utils.delay();
        PrtScreen();
        mainPage.verifyPageTitle();
            logger.log(LogStatus.PASS, "Parfumuri&cosmetice page verification !");
        mainPage.clickOnFirstSearchResult();
        PrtScreen();
        mainPage.detailsPageVerification();
            logger.log(LogStatus.PASS, "Details page, add to chart button verification !");
        PrtScreen();

    }
}
