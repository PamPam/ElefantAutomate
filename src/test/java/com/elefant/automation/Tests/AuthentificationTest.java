package com.elefant.automation.Tests;

import com.elefant.automation.Pages.Authentification;
import com.elefant.automation.Pages.MainPage;
import com.elefant.automation.Utils.BaseTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class AuthentificationTest extends BaseTest {


    @Test
    public void AuthentificationTest() {

        logger = extent.startTest("AuthentificationTest");

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        Authentification authentific = PageFactory.initElements(driver, Authentification.class);

        PrtScreen();
        mainPage.backToPageButton();
        PrtScreen();
        mainPage.clickOnContulMeu();
            logger.log(LogStatus.PASS, "Button \"Contul Meu\" is displayed !");
        PrtScreen();
        mainPage.clickOnIntraInCont();
            logger.log(LogStatus.PASS, "Authentification page is loaded !");
        PrtScreen();
        authentific.insertElefantData();
            logger.log(LogStatus.PASS, "Authentification data is inserted succesfully !");
        PrtScreen();
        authentific.clickOnLogin();
            logger.log(LogStatus.PASS, "Log in button is displayed !");
        PrtScreen();
        authentific.clickOnMainLogo();
        PrtScreen();
        mainPage.backToPageButton();
            logger.log(LogStatus.PASS, "Title verified !");
        PrtScreen();
        mainPage.verifyLogedUser();
            logger.log(LogStatus.PASS, "Logged user verification !");
        PrtScreen();
        mainPage.logout();
        PrtScreen();

    }
}
