package com.elefant.automation.Utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.util.Random;


public class Utils {

    /**
     *thred sleep
     * @param : 1 sec
     */
    public static void delay() {
        try {
            Thread.sleep(1000);
        }
        catch (Exception ex) {
        }
    }
    /**
    *thred sleep
    * if need it specific param will be added
     */
    public static void delay(int time) {
        try {
            Thread.sleep(time);
        }
        catch (Exception ex) {
        }
    }
    /**
    *scroll down
    * scroll down the page with 500 pixels
     */
    public static void scrollDown(WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,500)", "");
    }
    /**
     *Hoover the login area to allow the login button to appear and become clickable
     */
    public static void hoverButton(WebDriver d, WebElement area){
        delay();
        Actions DriverUsage = new Actions(d);
        DriverUsage = DriverUsage.moveToElement(area);
        Action action = DriverUsage.build();
        action.perform();
    }
    /**
     *Method to generate a random number between 2 values of min and max
     */
    public static int random(int mininm, int maxim){
        Random rand = new Random();
        int randomNum = rand.nextInt((maxim - mininm) + 1) + mininm;
        return randomNum;
    }
    /**
     * Create a folder to put all print-screen picture files
     */
    public static String createFolder(String x) {
        try {
            new File(x).mkdirs();
        }
        catch(Exception ex){
            System.out.println("Folder already exists !");
        }
        return x;
    }

    public static String CurrentWorkingDirectory(){
        String cwd = System.getProperty("user.dir");
        return cwd;
    }


}
