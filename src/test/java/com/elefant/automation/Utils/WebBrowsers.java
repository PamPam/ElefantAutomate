package com.elefant.automation.Utils;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WebBrowsers {


        public enum Browsers {
            CHROME,
            FIREFOX,
            IE,
            HTMLUNIT
        }

        public static org.openqa.selenium.WebDriver getDriver(Browsers browser) throws Exception {
            org.openqa.selenium.WebDriver driver = null;

            switch (browser) {

                case CHROME: {
                    System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\drivers\\chromedriver.exe");
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("start-maximized");
                    return new ChromeDriver(options);
                }
                default: {
                }
            }
            return driver;
        }
    }
