package com.elefant.automation.Utils;

import com.relevantcodes.extentreports.ExtentReports;

    public class ExtentManager {
        private static ExtentReports extent;
        private static ExtentReports instance;

        public static synchronized  ExtentReports getReporter(String filePath) {
            if (extent == null) {
                extent = new ExtentReports(filePath, true);

                extent
                        .addSystemInfo("Host Name", "Anshoo")
                        .addSystemInfo("Environment", "QA");
            }
            return extent;
        }
        public static ExtentReports getInstance() {
            return instance;
        }
    }
