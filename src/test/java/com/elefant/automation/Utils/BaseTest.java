package com.elefant.automation.Utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import static com.elefant.automation.Utils.WebBrowsers.Browsers.CHROME;


public class BaseTest {

    public static WebDriver driver;
    public static ExtentReports extent;
    public static ExtentTest logger;
    final String filePath = "Extent.html";


    public static void PrtScreen(){
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
        Date date2 = new Date();
        File finalFile = new File(Utils.CurrentWorkingDirectory()+"\\PictureReports\\"+dateFormat2.format(date2)+".jpg");
        try {
            FileUtils.copyFile(screenshotFile, finalFile);
        }
        catch (IOException IO){
            System.out.println("Error saving PrintScreen !");
        }
    }


    @BeforeSuite
    public void beforeSuite() {
        extent = ExtentManager.getReporter(filePath);
        extent = ExtentManager.getInstance();
    }

    @BeforeTest
    public void OpenDriver() {
        try {
            driver = WebBrowsers.getDriver(CHROME);
            driver.navigate().to("http://www.elefant.ro");
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

            DateFormat dateFormat2 = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            Date date2 = new Date();

            extent = new ExtentReports(System.getProperty("user.dir")+"/ExtentReport"+dateFormat2.format(date2)+".html", false);
            extent.addSystemInfo("Host Name", "Lenovo 80LM")
                    .addSystemInfo("Environment", "Chrome browser")
                    .addSystemInfo("User Name", "Dragos");
            extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
        } catch (Exception e) {
            System.out.println("WebDriver does not work");
        }
    }

    @AfterMethod
    protected void getResult(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
            logger.log(LogStatus.FAIL, "Test Case Failed is " + result.getName());
            logger.log(LogStatus.FAIL, "Test Case Failed is " + result.getThrowable());
        } else if (result.getStatus() == ITestResult.SKIP) {
            logger.log(LogStatus.SKIP, "Test Case skipped is "+result.getName());
            logger.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
        } else {
            logger.log(LogStatus.PASS, "Test passed");
        }
        extent.endTest(logger);
    }

    @AfterTest
    public void closeDriver() {
        Utils.delay();
        extent.flush();
        extent.close();
    }

    @AfterSuite
    protected void afterSuite() {
        Utils.delay();
       driver.close();
        Utils.delay();
        driver.quit();
    }
}